import math
import random
from xlrd import open_workbook, xldate_as_tuple
from xlwt import Workbook, XFStyle

wb = Workbook()

class MovementManager(object):
	"""docstring for MovementManager"""
	def __init__(self):
		self.average_bid = 1.
		self.probability_losing_bid = 0.
		self.percent_high_vot_drivers = 0.
		self.account_balance = 5.
		self.last_fifty_bids = []
		self.last_fifty_bid_decisions = [] # 1 --> win, 0 --> loss
		self.results = {}
		self.current_bid = 0.
		self.current_bid_decision = 0.
		self.queue_lenth = 0.
		self.new_arrivals = 0.
		self.voluntary_contributions = 0.
		self.bid_less_than_one_flag = 0.

	def compute_average_bid(self):
		if len(self.last_fifty_bids) != 0:
			self.average_bid = round(sum(self.last_fifty_bids)/len(self.last_fifty_bids), 2)

	def compute_current_bid(self):

		self.current_bid = round(self.average_bid * (0.5 + self.probability_losing_bid) * (1 + self.percent_high_vot_drivers), 2)

		if self.current_bid < 1:
			self.current_bid = 1.
			self.less_than_one_flag = 1
		else:
			self.less_than_one_flag = 0

		if self.account_balance - self.current_bid < 0:
			self.current_bid = max(self.account_balance - 1, 1)

	def simulate_queue(self):
		self.percent_high_vot_drivers = round(random.uniform(0, 1), 2)
		self.voluntary_contributions = round(self.percent_high_vot_drivers * (1+round(random.uniform(0,1), 2)), 2)
		self.account_balance += self.voluntary_contributions

	def update_last_fifty_bids(self):
		self.last_fifty_bids.append(self.current_bid)
		if len(self.last_fifty_bids) > 50:
			self.last_fifty_bids.pop(0)
		
	def decide_winning_bid(self):
		random_propability = round(random.uniform(0, 1), 2)
		if random_propability > 0.5:
			self.current_bid_decision = 1
		else:
			self.current_bid_decision = 0

	def update_last_fifty_bid_decision(self):
		self.last_fifty_bid_decisions.append(self.current_bid_decision)
		if len(self.last_fifty_bid_decisions) > 50:
			self.last_fifty_bid_decisions.pop(0)

	def compute_probability_of_losing(self):
		if len(self.last_fifty_bid_decisions) != 0:
			prob_win = round(sum(self.last_fifty_bid_decisions)/float(len(self.last_fifty_bid_decisions)), 2)
			self.probability_losing_bid = 1 - prob_win

	def update_parameters(self):
		self.update_financial_transactions()
		self.update_last_fifty_bids()
		self.update_last_fifty_bid_decision()
		self.compute_probability_of_losing()
		self.compute_average_bid()

	def update_financial_transactions(self):
		if self.current_bid_decision == 1:
			self.account_balance -= self.current_bid

		if self.account_balance < 1:
			self.account_balance += 1

	def main(self):
		self.simulate_queue()
		self.compute_current_bid()
		self.decide_winning_bid()		

	def get_current_simulation_parameters(self):
		return [self.bid_less_than_one_flag, self.current_bid, self.average_bid, self.voluntary_contributions, self.account_balance, 
		self.percent_high_vot_drivers, self.current_bid_decision, self.probability_losing_bid]
		

movement_manager = MovementManager()

def monte_carlo_simulation():
	for i in xrange(300000):
		movement_manager.main()
		value = movement_manager.get_current_simulation_parameters()
		movement_manager.results[i+1] = value
		movement_manager.update_parameters()

def write_along_column(sheet, vals, r, c = 0):
    for i in xrange(len(vals)):
        sheet.write(r, c+i, vals[i])

def write_results():
	r = 0
	i = 1
	_name = 'results_'+str(i)
	results = wb.add_sheet(str(_name))
	results.write(0, 1, 'number')
	results.write(0, 2, 'bid_less_than_one_flag')
	results.write(0, 3, 'current_bid')
	results.write(0, 4, 'average_bid')
	results.write(0, 5, 'voluntary_contributions')
	results.write(0, 6, 'account_balance')
	results.write(0, 7, 'percent_high_vot_drivers')
	results.write(0, 8, 'current_bid_decision')
	results.write(0, 9, 'probability_losing_bid')

	for (number, value) in movement_manager.results.items():
		results.write(r+1, 1, number)
		for j in xrange(len(value)):
			results.write(r+1, j+2, value[j])

		r += 1

		if r > 65000:
			i += 1
			r = 0
			_name = 'results_'+str(i)
			results = wb.add_sheet(str(_name))
			results.write(0, 1, 'number')
			results.write(0, 2, 'bid_less_than_one_flag')
			results.write(0, 3, 'current_bid')
			results.write(0, 4, 'average_bid')
			results.write(0, 5, 'voluntary_contributions')
			results.write(0, 6, 'account_balance')
			results.write(0, 7, 'percent_high_vot_drivers')
			results.write(0, 8, 'current_bid_decision')
			results.write(0, 9, 'probability_losing_bid')

	wb.save('results.xls')

def main():
	monte_carlo_simulation()
	write_results()
		